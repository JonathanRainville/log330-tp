-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 27 Juillet 2015 à 15:00
-- Version du serveur: 5.5.41-cll-lve
-- Version de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `log330Labo`
--

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `addAnnotation`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `addAnnotation`(IN `annotName` VARCHAR(50) CHARSET utf8, IN `annotType` INT(11), IN `annotDate` DATE, IN `annotCourse` INT(11), IN `annotUser` INT(11), IN `annotPriority` INT(11), IN `annotDescription` TEXT CHARSET utf8, IN `annotCompleted` BOOLEAN)
    NO SQL
BEGIN
INSERT INTO annotations (name, type_id, deadline, course_id, user_id, priority_id, description, completed) VALUES (annotName, annotType, annotDate, annotCourse, annotUser, annotPriority, annotDescription, annotCompleted);
SELECT annotation_id FROM annotations WHERE name = annotName AND type_id = annotType AND deadline = annotDate AND course_id = annotCourse AND user_id = annotUser AND priority_id = annotPriority AND description = annotDescription;
END$$

DROP PROCEDURE IF EXISTS `addUser`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `addUser`(IN `newChromeId` VARCHAR(155))
BEGIN
INSERT INTO `users` (`chromeId`) VALUES (newChromeId);
SELECT user_id FROM users WHERE chromeId = newChromeId;
END$$

DROP PROCEDURE IF EXISTS `deleteAnnotation`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `deleteAnnotation`(IN `annotId` INT(50))
    NO SQL
DELETE FROM annotations WHERE annotation_id = annotId$$

DROP PROCEDURE IF EXISTS `editAnnotation`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `editAnnotation`(IN `annotName` VARCHAR(50) CHARSET utf8, IN `annotType` INT(11), IN `annotDate` DATE, IN `annotCourse` INT(11), IN `annotUser` INT(11), IN `annotPriority` INT(11), IN `annotDescription` TEXT CHARSET utf8, IN `annotCompleted` BOOLEAN, IN `annotationId` INT)
    NO SQL
UPDATE annotations
SET name = annotName, type_id = annotType, deadline = annotDate, course_id = annotCourse, priority_id = annotPriority, description = annotDescription, completed = annotCompleted
WHERE annotation_id = annotationId$$

DROP PROCEDURE IF EXISTS `getConcentrations`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getConcentrations`()
    NO SQL
SELECT * FROM concentrations ORDER BY name$$

DROP PROCEDURE IF EXISTS `getCourses`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getCourses`()
    NO SQL
SELECT * FROM courses ORDER BY name$$

DROP PROCEDURE IF EXISTS `getPriorities`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getPriorities`()
    NO SQL
SELECT *
FROM priority$$

DROP PROCEDURE IF EXISTS `getSchools`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getSchools`()
    NO SQL
SELECT * FROM schools ORDER BY name$$

DROP PROCEDURE IF EXISTS `getTypes`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getTypes`()
    NO SQL
SELECT * FROM types$$

DROP PROCEDURE IF EXISTS `getUserAnnotations`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getUserAnnotations`(IN `userId` INT(11))
    NO SQL
SELECT * 
FROM annotations 
WHERE user_id = userID
ORDER BY name$$

DROP PROCEDURE IF EXISTS `getUserCourses`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getUserCourses`(IN `user_id` INT(11))
SELECT c.name, c.course_id, c.concentration_id
FROM courses AS c
LEFT JOIN userscourses AS a
ON c.course_id = a.course_id
WHERE a.user_id = user_id
ORDER BY c.name$$

DROP PROCEDURE IF EXISTS `getUserId`$$
CREATE DEFINER=`cpses_dyZ2Mnjv8r`@`localhost` PROCEDURE `getUserId`(IN `chromeId` VARCHAR(255) CHARSET utf8)
    NO SQL
SELECT user_id
FROM users
WHERE users.chromeId = chromeId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `annotations`
--

DROP TABLE IF EXISTS `annotations`;
CREATE TABLE IF NOT EXISTS `annotations` (
  `annotation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `type_id` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`annotation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `annotations`
--

INSERT INTO `annotations` (`annotation_id`, `name`, `type_id`, `deadline`, `course_id`, `user_id`, `priority_id`, `description`, `completed`) VALUES
(20, 'Examen    intra', 2, '2015-07-30', 3, 12, 1, '%C3%89tudier  %231  et  2', 0),
(21, 'Labo  2', 3, '2015-07-27', 4, 12, 2, '', 0),
(22, 'Devoir', 1, '2015-07-27', 1, 12, 3, '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `concentrations`
--

DROP TABLE IF EXISTS `concentrations`;
CREATE TABLE IF NOT EXISTS `concentrations` (
  `concentration_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`concentration_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `concentrations`
--

INSERT INTO `concentrations` (`concentration_id`, `name`, `school_id`) VALUES
(1, 'Génie Logiciel', 1);

-- --------------------------------------------------------

--
-- Structure de la table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `concentration_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `courses`
--

INSERT INTO `courses` (`course_id`, `name`, `concentration_id`) VALUES
(1, 'LOG240', 1),
(3, 'LOG330', 1),
(4, 'LOG210', 1),
(5, 'LOG121', 1);

-- --------------------------------------------------------

--
-- Structure de la table `priority`
--

DROP TABLE IF EXISTS `priority`;
CREATE TABLE IF NOT EXISTS `priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `priority`
--

INSERT INTO `priority` (`priority_id`, `name`) VALUES
(1, 'Élevée'),
(2, 'Modérée'),
(3, 'Faible');

-- --------------------------------------------------------

--
-- Structure de la table `schools`
--

DROP TABLE IF EXISTS `schools`;
CREATE TABLE IF NOT EXISTS `schools` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `schools`
--

INSERT INTO `schools` (`school_id`, `name`) VALUES
(1, 'ÉTS');

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `types`
--

INSERT INTO `types` (`type_id`, `name`) VALUES
(1, 'Devoir'),
(2, 'Examen'),
(3, 'Note');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `chromeId` varchar(155) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`user_id`, `chromeId`) VALUES
(12, '108519980665974445430');

-- --------------------------------------------------------

--
-- Structure de la table `userscourses`
--

DROP TABLE IF EXISTS `userscourses`;
CREATE TABLE IF NOT EXISTS `userscourses` (
  `usersCourse_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`usersCourse_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `userscourses`
--

INSERT INTO `userscourses` (`usersCourse_id`, `user_id`, `course_id`) VALUES
(2, 12, 3),
(3, 12, 4),
(16, 12, 5),
(17, 12, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
