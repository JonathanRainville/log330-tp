/*global Handlebars, console, _*/
'use strict';

// Helper to format a date
// date 		{Date|String}	The date to format
// separator 	{String} 		The separator between the date parts
// format 		{String} 		The format to use
Handlebars.registerHelper('formatDate', function (date, separator, format) {
	// If the date is a string, we convert it to a DateObject
	if (typeof date === 'string') {
		var arr = date.split('-');
		var d = arr[2];
		var m = arr[1];
		var y = arr[0];
		date = new Date();
		date.setDate(parseInt(d));
		date.setMonth(parseInt(m)-1);
		date.setFullYear(parseInt(y));
	}

	// Default separator is '-'
	if (typeof separator !== 'string') {
		separator = '-';
	}

	// Month variable with starting 0
	var month = (date.getMonth() + 1);
	if (month < 10) {
		month = '0' + month;
	}

	// Day variable with starting 0
	var day = date.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	// If the format is specified, create right format
	if (format) {
		var formatArr = format.split(separator);
		var display = '';
		_.each(formatArr, function (part) {
			switch (part.toLowerCase()) {
				case 'y': display += date.getFullYear() + separator; break;
				case 'm': display += month + separator; break;
				case 'd': display += day + separator; break;
			}
		});
		return display.substr(0, display.length - separator.length);
	}

	// Return default format
	return day + separator + month + separator + date.getFullYear();
});

// Compares two values
Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
    if (arguments.length < 3){
        throw new Error('Handlebars Helper equal needs 2 parameters');
    }
    if( lvalue!=rvalue ) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    }
});