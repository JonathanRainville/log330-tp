/*global console, chrome, $*/
'use strict';

var userIdPromise = $.Deferred();

// Server path
var AJAX_PATH = 'http://oubliets.doyouevenweb.com/PHP/ajax/';

// Local path
// var AJAX_PATH = 'http://localhost/log330/PHP/ajax/';

chrome.identity.getProfileUserInfo(function (userInfo){
	if (userInfo.id) {
		$.get(AJAX_PATH + 'getUserId.php', {chromeId: userInfo.id}, function (id) {
	        userIdPromise.resolve(id);
	    });
	} else {
		userIdPromise.reject('User not logged in');
	}
});