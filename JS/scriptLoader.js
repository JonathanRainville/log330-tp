/*global $*/

/**
* Loads a script in the head of the file while making sure it isn't already loaded
* 
* @param url        String      The url of the file
* @param callback   Function    Function to call after the file load
*/
function loadScript (url, callback) {
    'use strict';
    // Adding the script tag to the head
    var head = $('head');
    if (head.find('script[src="' + url + '"]').length) {
        if (callback) {
            callback();
        }
        return;
    }
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    if (callback) {
        script.onreadystatechange = callback;
        script.onload = callback;
    }

    // Fire the loading
    head[0].appendChild(script);
}