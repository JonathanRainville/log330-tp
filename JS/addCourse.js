﻿/*global console, $, loadScript, Handlebars, Course, Main, _, AJAX_PATH, userIdPromise*/
'use strict';

function initAddCourse () {
    // We get the template
    $.get('JS/templates/addCourseTmpl.hbs', function(data) {
        var template = Handlebars.compile(data);
        var context = {schools: []};
        $.get(AJAX_PATH + 'getAllCourses.php', function (totalData) {
        	var courses = totalData.courses;
        	var concentrations = totalData.concentrations;
        	var schools = totalData.schools;

        	var groupedCourses = _.groupBy(courses, 'concentration_id');
        	var groupedConcentrations = _.indexBy(concentrations, 'school_id');

        	_.each(groupedConcentrations, function (faculties, school_id) {
        		context.schools.push({
        			school_id: school_id,
        			name: _.findWhere(schools, {school_id: school_id + ''}).name,
        			faculties: []
        		});

        		if (typeof faculties === 'object') {
        			var c = [];
        			c.push(faculties);
        			faculties = c;
        		}

        		_.each(faculties, function (concentration) {
        			var id = concentration.concentration_id;

                    // Check if course is selected already
                    _.each(groupedCourses[id], function (course) {
                        if (_.findWhere(Main.courses, {course_id: course.course_id})) {
                            course.selected = true;
                        }
                    });

        			context.schools[context.schools.length - 1].faculties.push({
        				faculty_id: id,
        				name: concentration.name,
        				courses: groupedCourses[id]
        			});
        		});
        	});


    	    // We tenplate and show
    	    var html = template(context);
    	    $('#content').html(html);
        }, 'json');
    });
}

var chosenCourses = _.map(Main.courses, function(course){ 
    return course.course_id; 
});

$('#content').on('click', '#addCourseForm .course', function(evt) {
	var id = $(this).attr('data-id');
	if ($(this).attr('aria-expanded') === 'true') {
		// S'il était déjà choisi, on l'enlève du array
        if (_.findWhere(Main.annotations, {course_id: id})) {
            var collapseObj = $(this).closest('.panel').find('.panel-collapse');
            collapseObj.on('hidden.bs.collapse', function () {
                collapseObj.collapse('show');
            });

            $(this).popover({
                title: 'Impossible de retirer ce cours',
                content: 'Ce cours contient une ou plusieurs annotations, supprimez d\'abord celles-ci.',
                placement: 'top',
                trigger: 'hover'
            })
            .popover('show')
            .on('hidden.bs.popover', function() {
                $(this).popover('destroy');
            });
            return;
        }
		chosenCourses = _.without(chosenCourses, id);
		return;
	}
	chosenCourses.push(id);
});

// Handle save action
$('#content').on('click', '#saveCourse', function(evt) {
    evt.preventDefault();

    if (chosenCourses.length) {
        userIdPromise.done(function (userId) {
    	    $.get(
    	    	AJAX_PATH + 'addCourses.php', 
    	    	{user_id: userId, courses: JSON.stringify(chosenCourses)}, 
    	    	function (courses) {
    	    		Main.editCourses(courses);
    	    		Main.reinitialize();
    	    	}, 'json');
    	});
    } else {
        Main.reinitialize();
    }
});

var canceling = false;

// Handle cancel action
$('#content').on('click', '#cancelCourse', function(evt) {
    evt.preventDefault();

    // The popover is still active, we really cancel
    if (canceling) {
        $(this).popover('destroy');
        Main.reinitialize();
        return;
    }

    //Initialize the popover
    $(this).popover({
            title: 'Êtes-vous sûr?',
            content: 'Cliquez à nouveau sur ce bouton pour confirmer, sinon, cliquez ailleurs.',
            placement: 'top',
            trigger: 'focus'
        })
        .popover('show')
        .on('hidden.bs.popover', function() {
            canceling = false;
            $(this).popover('destroy');
        });

    canceling = true;
});