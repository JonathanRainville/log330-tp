﻿/*global console, $, loadScript, Handlebars, _, AnnotationModule, userIdPromise, AJAX_PATH*/
'use strict';

//0 is default, 1 is date, 2 is priority
var lastSortChoice = 0;
var DELAY = 250, clicks = 0, timer = null;

$(function () {

	var content = $('#content');

	userIdPromise.done(function (userId) {
		var annotationsPromise = $.Deferred();
		var coursesPromise = $.Deferred();

	    $.get(AJAX_PATH + 'getUserAnnotations.php', {user_id: userId}, function (data) {
	    	annotationsPromise.resolve(data);
		}, 'json');

		$.get(AJAX_PATH + 'getUserCourses.php', {user_id: userId}, function (data) {
			coursesPromise.resolve(data);
		}, 'json');

		$.when(annotationsPromise, coursesPromise).done(function (annotations, courses) {
			_.each(annotations, function (a) {
				a.completed = parseInt(a.completed);
			});

			Main.annotations = annotations;
			Main.courses = courses;

			Main.reinitialize();
		});
	});

	userIdPromise.fail(function (err) {
		content.empty().html('<p>Désolé, vous n\'êtes pas connecté à Chrome. Allez dans les paramètres et connectez vous afin d\'avoir accès à l\'extension.</p>');
	});

	// On click of the Add Annotation Button
	content.on('click', '#addAnnotation', function (evt) {
		evt.preventDefault();
		loadScript('JS/addAnnotation.js', function () {
			AnnotationModule.addAnnotation();
		});
	});
	
	// On click of the "x" icon 
	$('body').on('click', '.hasicon > .icon', function() {
		$(this).parent().remove();

		Main.deleteAnnotation($(this).closest('div').find('a.annotation').attr('data-id'), true);
	});
	
	// On click of the Sort Date Button
	$('#content').on('click', '#sortDate', function (evt) {
		if (lastSortChoice != 1){
			evt.preventDefault();
			Main.sortByDate();
		}
	});
	
	// On click of the Sort Priority Button
	$('#content').on('click', '#sortPriority', function (evt) {
		if (lastSortChoice != 2){
			evt.preventDefault();
			Main.sortByPriority();
		}
	});
	
	// On click of the Regular List Button
	$('#content').on('click', '#regularList', function (evt) {
		if (lastSortChoice !== 0){
			evt.preventDefault();
			Main.showDefaultList();
		}
	});
	
	// On click of the Add Course Button
	content.on('click', '#addCourse', function (evt) {
		evt.preventDefault();
		loadScript('JS/addCourse.js', function () {
			initAddCourse();
		});
	});

	$('#content').on('click', '.annotation', function (evt) {

        clicks++;  //count clicks

        var annotationId = $(this).attr('data-id');
        var annotation = _.findWhere(Main.annotations, {annotation_id: annotationId});

        if(clicks === 1) {

            timer = setTimeout(function() {

                evt.preventDefault();
				loadScript('JS/addAnnotation.js', function () {
					AnnotationModule.addAnnotation(annotation);
				});
				
                clicks = 0;             //after action performed, reset counter

            }, DELAY);

        } else {

            clearTimeout(timer);    //prevent single-click action
            
            if (annotation.completed) {
            	annotation.completed = 0;
            } else {
            	annotation.completed = 1;
            }

            $.get(AJAX_PATH + 'addAnnotation.php', {annotation: annotation, editMode: true});
            
            clicks = 0;
			
			Main.reinitialize();
        }

    })
    .on('dblclick', function(e){
        e.preventDefault();  //cancel system double-click event
    });

});



var Main = Main || {};

Main.reinitialize = function () {

	$('#content').empty();
	
	switch(lastSortChoice) {
    case 0:
        Main.showDefaultList();
        break;
    case 1:
        Main.sortByDate();
        break;
    case 2:
        Main.sortByPriority();
		break;
	}
};

Main.showDefaultList = function () {
	lastSortChoice = 0;
	
	$.get('JS/templates/annotationListTmpl.hbs', function (data) {
		var template = Handlebars.compile(data);

		var groupedAnnots = _.groupBy(Main.annotations, 'course_id');
		var context = {courses: []};

		_.each(groupedAnnots, function (annots, course_id) {
			if (!_.isArray(annots)) {
				var t = [];
				t.push(annots);
				annots = t;
			}

			context.courses.push({
				name: _.findWhere(Main.courses, {course_id: course_id}).name,
				annotations: annots
			});
		});

		var html = template(context);
		$('#content').html(html);
	});
	
};

Main.sortByDate = function () {
	lastSortChoice = 1;
	
	$.get('JS/templates/sortedAnnotationList.hbs', function (data) {
		var template = Handlebars.compile(data);

		var context = {annotations: []};
		var cloned = JSON.parse(JSON.stringify(Main.annotations));
		context.annotations	= cloned.sort(function(a, b){
		 var dateA = new Date(a.deadline), dateB = new Date(b.deadline);
		 return dateA - dateB; //sort by date ascending
		});

		_.each(context.annotations, function (annotation) {
			annotation.courseName = _.findWhere(Main.courses, {course_id: annotation.course_id}).name;
		});

		var html = template(context);
		$('#content').html(html);
	});
};

Main.sortByPriority = function () {
	lastSortChoice = 2;
	
	$.get('JS/templates/sortedAnnotationList.hbs', function (data) {
		var template = Handlebars.compile(data);

		var context = {annotations: []};
		var cloned = JSON.parse(JSON.stringify(Main.annotations));
		context.annotations	= cloned.sort(function(a, b){
		return  b.priority_id - a.priority_id;
		}).reverse();
		
		_.each(context.annotations, function (annotation) {
			annotation.courseName = _.findWhere(Main.courses, {course_id: annotation.course_id}).name;
		});
		
		var html = template(context);
		$('#content').html(html);
	});
};

Main.addAnnotation = function (annotation) {
	Main.annotations.push(annotation);
};

Main.editAnnotation = function (annotation) {
	Main.deleteAnnotation(annotation.annotation_id);
	Main.annotations.push(annotation);
};

Main.deleteAnnotation = function (annotation_id, reallyDelete) {
	Main.annotations = _.reject(Main.annotations, function(annot){ return annot.annotation_id === annotation_id; });

	if (reallyDelete) {
		$.get(AJAX_PATH + 'deleteAnnotation.php', {annotation_id: annotation_id}, function (prout) {
			console.log(prout);
		});
	}
};

Main.editCourses = function (courses) {
	Main.courses = courses;
};