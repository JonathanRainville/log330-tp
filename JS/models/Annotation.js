'use strict';
var Annotation = function () {
    this.name = '';
    this.description = '';
    this.type_id = -1;
    this.deadline = new Date();
    this.priority_id = -1;
    this.course_id = -1;
    this.user_id = -1;
    this.completed = 0;
};