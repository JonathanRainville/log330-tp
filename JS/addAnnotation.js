﻿/*global console, $, loadScript, Handlebars, Annotation, Main, AJAX_PATH, _, userIdPromise*/
'use strict';

var annot;

var AnnotationModule = {};

AnnotationModule.addAnnotation = function (annotation) {

	AnnotationModule.editMode = false;
	if (annotation) {
		AnnotationModule.editMode = true;
	}
	
	// We get the template
	$.get('JS/templates/addAnnotationTmpl.hbs', function (data) {
		var template = Handlebars.compile(data);
		// We load the annotation model
		loadScript('JS/models/Annotation.js', function () {
			annot = annotation || new Annotation();
			var context = _.extend({}, annot);

			var typesPromise = $.Deferred();
			var prioritiesPromise = $.Deferred();
			var coursesPromise = $.Deferred();
			 
			$.when( typesPromise, prioritiesPromise, coursesPromise ).done(function ( types, priorities, courses ) {
			    context.types = types;
			    context.priorities = priorities;

				context.courses = courses;

				// We tenplate and show
				var html = template(context);
				$('#content').html(html);

			});


			$.get(AJAX_PATH + 'getTypes.php', function (c) {
                typesPromise.resolve(c);
			}, 'json');

			userIdPromise.done(function (userId) {
				$.get(AJAX_PATH + 'getUserCourses.php', {user_id: userId}, function (t) {
					coursesPromise.resolve(t);
				}, 'json');
			});


			$.get(AJAX_PATH + 'getPriorities.php', function (p) {
				prioritiesPromise.resolve(p);
			}, 'json');
		});
	});
};

// Handle save action
$('#content').on( 'click', '#addAnnotationForm #saveAnnotation', function(evt) {
	evt.preventDefault();

	var formValues = $('#addAnnotationForm').serialize().split('&');
	_.each(formValues, function (value) {
		// I split the vale. Index 0 is the name, Index 1 is the value itself
		var splittedValue = value.split('=');
		// We fill our model
		annot[splittedValue[0]] = splittedValue[1].replace(/\+/g, '  ');
	});

	userIdPromise.done(function (userId) {
        annot.user_id = userId;
    });

	$.get(AJAX_PATH + 'addAnnotation.php', {annotation: annot, editMode: AnnotationModule.editMode}, function(id) {
		if (AnnotationModule.editMode) {
			Main.editAnnotation(annot);
		}else {
			annot.annotation_id = id;
			Main.addAnnotation(annot);
		}
		Main.reinitialize();
	});
});

var canceling = false;

// Handle cancel action
$('#content').on('click', '#cancelAnnot', function(evt) {
	evt.preventDefault();

	// The popover is still active, we really cancel
	if (canceling) {
		$(this).popover('destroy');
		Main.reinitialize();
		return;
	}

	//Initialize the popover
	$(this).popover({
		title: 'Êtes-vous sûr?',
		content: 'Cliquez à nouveau sur ce bouton pour confirmer, sinon, cliquez ailleurs.',
		placement: 'top',
		trigger: 'focus'
	})
	.popover('show')
	.on('hidden.bs.popover', function () {
		canceling = false;
		$(this).popover('destroy');
	});

	canceling = true;
});