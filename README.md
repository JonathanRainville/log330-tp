# README #

### OubliETS ###

Agenda électronique idéal pour toute personne fréquentant l'ÉTS.

### Comment l'utiliser en mode développement? ###

* Cloner le projet / Check out la branche voulue
* Ouvrir wampserver
* Créer un alias pointant sur le dossier de votre projet local
* Importer la base de données nommée "log330.sql" en la nommant "log330"
* Ouvrir Google Chrome et aller dans les paramètre, sous l'onglet extensions
* "Drag and drop" le dossier local du projet dans la fenêtre d'extension
* Voila, vous être prêt à développer et tester