<?php

class BDD
{
	protected $_bdd;
	
	public function __construct(){}
	
	/**
	 * Function initializaBDD : initialize the database
	 * @return $_bdd
	 */
	protected static function initializeBDD()
	{
		try
		{
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$pdo_options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES utf8";

			$_bdd = new PDO('mysql:host=localhost;dbname=log330', 'root', '', $pdo_options);
		}
		catch (Exception $ex)
		{
			die('Une erreur est survenue: ' . $ex->getMessage());
		}
		return $_bdd;
	}
	

	public static function getBDD()
	{
		if (isset($_bdd)) {
			return $_bdd;
		}
		return self::initializeBDD();
	}
}

?>