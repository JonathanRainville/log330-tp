<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();


	$sql =  'CALL getTypes()';
	$stmt = $conn->query($sql);
	$types = $stmt->fetchAll();

  	echo json_encode($types);
?>