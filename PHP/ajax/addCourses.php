<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	$courses = json_decode($_GET['courses']);
	$user_id = $_GET['user_id'];

	$sql =  "DELETE FROM userscourses WHERE user_id = '$user_id'";
	$conn->exec($sql) or die(print_r($conn->errorInfo(), true));

	if ($courses && count($courses) > 0) {
		$sql =  "INSERT INTO userscourses (user_id, course_id) VALUES";

		for ($i = 0; $i < count($courses); $i++) {
			$course_id = $courses[$i];
			$sql .= " ($user_id, $course_id),";
		}

		$sql = substr($sql, 0, strlen($sql)-1);

		$conn->exec($sql) or die(print_r($conn->errorInfo(), true));
	}

	$sql =  "CALL getUserCourses($user_id)";
	$stmt = $conn->query($sql) or die(print_r($conn->errorInfo(), true));
	$courses = $stmt->fetchAll();

  	echo json_encode($courses);
?>