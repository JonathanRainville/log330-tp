<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	$sql =  'CALL getCourses()';
	$stmt = $conn->query($sql);
	$courses = $stmt->fetchAll();

	$stmt->closeCursor();
	$sql =  'CALL getConcentrations()';
	$stmt = $conn->query($sql);
	$concentrations = $stmt->fetchAll();

	$stmt->closeCursor();
	$sql =  'CALL getSchools()';
	$stmt = $conn->query($sql);
	$schools = $stmt->fetchAll();

	$result = array(
		'courses' => $courses,
		'concentrations' => $concentrations,
		'schools' => $schools
	);

  	echo json_encode($result);
?>