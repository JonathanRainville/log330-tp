<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	$user_id = $_GET['user_id'];

	$sql =  "CALL getUserCourses($user_id)";
	$stmt = $conn->query($sql);
	$courses = $stmt->fetchAll();

  	echo json_encode($courses);
?>