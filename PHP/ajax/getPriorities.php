<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();


	$sql =  'CALL getPriorities()';
	$stmt = $conn->query($sql);
	$priorities = $stmt->fetchAll();

  	echo json_encode($priorities);
?>