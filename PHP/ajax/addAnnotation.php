<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	$annotation = $_GET['annotation'];
	$name = $annotation['name'];
	$type = $annotation['type_id'];
	$date = $annotation['deadline'];
	$course = $annotation['course_id'];
	$user = $annotation['user_id'];
	$priority = $annotation['priority_id'];
	$description = $annotation['description'];
	$completed = $annotation['completed'];
	

	if (isset($_GET['editMode']) && $_GET['editMode'] == 'true') {
		$funct = 'editAnnotation';
	} else {
		$funct = 'addAnnotation';
	}

	if (isset($annotation['annotation_id'])) {
		$annotation_id = ", '" . $annotation['annotation_id'] . "'";
	} else {
		$annotation_id = '';
	}

	$sql =  "CALL $funct('$name', '$type', '$date', '$course', '$user', '$priority', '$description', '$completed'$annotation_id)";

	$stmt = $conn->query($sql);

	if (!isset($_GET['editMode']) || $_GET['editMode'] == 'false') {
		$row = $stmt->fetch();
		$id = $row['annotation_id'];

		echo $id;
	}
?>