<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	$user_id = $_GET['user_id'];

	$sql =  "CALL getUserAnnotations('$user_id')";
	$stmt = $conn->query($sql);
	$annots = $stmt->fetchAll();

  	echo json_encode($annots);
?>