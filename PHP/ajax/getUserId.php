<?php
	header("Access-Control-Allow-Origin: *");

	require_once('../BDD.class.php');

	$conn = BDD::getBDD();

	if (!isset($_GET['chromeId'])){
		echo false;
		exit;
	}

	$chromeId = $_GET['chromeId'];

	$sql =  "CALL getUserId('$chromeId')";
	$stmt = $conn->query($sql);
	$row = $stmt->fetch();
	$id;

	if (!$row) {

		$stmt->closeCursor();
		$sql = "CALL addUser('$chromeId')";
		$stmt = $conn->query($sql);
		$row = $stmt->fetch();
		$id = $row['user_id'];
	} else {
		$id = $row['user_id'];
	}

	echo $id;
?>